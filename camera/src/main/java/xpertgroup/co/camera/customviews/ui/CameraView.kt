package xpertgroup.co.camera.customviews.ui

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewTreeObserver
import android.widget.RelativeLayout
import com.camerakit.CameraKitView
import kotlinx.android.synthetic.main.camera_view.view.*
import xpertgroup.co.camera.R

class CameraView  @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : RelativeLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.camera_view, this, true)
        configCamera()
    }

    private fun configCamera(){
        containerCamera.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                camera.aspectRatio = containerCamera.height.toFloat()/containerCamera.width.toFloat()
                containerCamera.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })
    }

    fun capturePicture(handle  : (Bitmap)-> Unit){
        capturePictureArray{
            val bitmap = BitmapFactory.decodeByteArray(it, 0, it.size)
            handle.invoke(bitmap)
        }
    }

    private fun capturePictureArray(handle  : (ByteArray)-> Unit){
        camera.captureImage{ _: CameraKitView, bytes: ByteArray ->
            handle.invoke(bytes)
        }
    }

    fun onStart(){
        camera.onStart()
    }

    fun onResume(){
        camera.onResume()
    }

    fun onPause(){
        camera.onPause()
    }

    fun onStop(){
        camera.onStop()
    }

    fun onRequestPermissionResult(requestCode :Int, permissions : Array<out String>, grantResults: IntArray){
        camera.onRequestPermissionsResult(requestCode,permissions,grantResults)
    }
}