package xpertgroup.co.camerascannerqr.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewTreeObserver
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.camera_scanner_view.view.*
import xpertgroup.co.camerascannerqr.R


class CameraScannerView   @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : RelativeLayout(context, attrs, defStyleAttr){

   init {
        LayoutInflater.from(context).inflate(R.layout.camera_scanner_view, this, true)
   }

   private fun initView(){
       scannerContainer.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
           override fun onGlobalLayout() {

               val dimen = scannerContainer.height *0.5f
               val layoutParams = gosh.layoutParams
               layoutParams.height =dimen.toInt()
               layoutParams.width = dimen.toInt()
               gosh.layoutParams = layoutParams
               scannerContainer.viewTreeObserver.removeOnGlobalLayoutListener(this)
           }
       })
    }

}