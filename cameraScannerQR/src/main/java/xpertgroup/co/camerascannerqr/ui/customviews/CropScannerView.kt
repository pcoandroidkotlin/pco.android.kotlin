package xpertgroup.co.camerascannerqr.ui.customviews

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.widget.RelativeLayout
import xpertgroup.co.camerascannerqr.components.DaggerScannerComponent
import xpertgroup.co.camerascannerqr.exceptions.CropBitMapException
import xpertgroup.co.scannerqr.interfaces.IScannerQR
import javax.inject.Inject

class CropScannerView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : RelativeLayout(context, attrs, defStyleAttr) {

    private  var bitmapCrop : Bitmap? = null
    @Inject
    lateinit var scannerQR: IScannerQR

    init {
        val component = DaggerScannerComponent.builder().build()
        component.inject(this)
    }

    fun scan(bitmapPattern:Bitmap, handle : (String?, Boolean)-> Unit){
        try{
            bitmapCrop = Bitmap.createBitmap(
                bitmapPattern,
                Math.round(this.x),
                Math.round(this.y),
                this.width,
                this.height
            )
            scannerQR.scan(bitmapCrop){ value: String?, success: Boolean ->
                    handle.invoke(value,success)
            }
        }
        catch (ex : Exception){
            throw  CropBitMapException(ex.message!!,ex.cause)
        }
    }
}