package xpertgroup.co.camerascannerqr.exceptions

class CropBitMapException (message: String,cause:Throwable? = null ) : Exception( message, cause)
