package xpertgroup.co.camerascannerqr.components

import dagger.Component
import xpertgroup.co.camerascannerqr.ui.customviews.CropScannerView
import xpertgroup.co.scannerqr.interfaces.IScannerQR
import xpertgroup.co.scannerqr.modules.ScannerQRModule

@Component(modules = [ScannerQRModule::class])
interface ScannerComponent {
    fun scannerOcr(): IScannerQR
    fun inject(cropScannerView: CropScannerView)

}