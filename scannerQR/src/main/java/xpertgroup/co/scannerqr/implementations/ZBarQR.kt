package xpertgroup.co.scannerqr.implementations

import android.graphics.Bitmap
import net.sourceforge.zbar.Config
import net.sourceforge.zbar.Image
import net.sourceforge.zbar.ImageScanner
import net.sourceforge.zbar.Symbol
import xpertgroup.co.scannerqr.interfaces.IScannerQR
import java.lang.Exception

class ZBarQR : IScannerQR {

    private var scanner: ImageScanner? = null

    init{
        scanner = ImageScanner()
        scanner?.setConfig(0, Config.ENABLE, 0)
        scanner?.setConfig(Symbol.QRCODE, Config.ENABLE, 1)
    }

    override fun scan(bitmap: Bitmap?, handle: (String?, Boolean) -> Unit) {
        try {
            if(bitmap != null){
                val width = bitmap.width
                val height = bitmap.height
                val pixels = IntArray(width * height)
                bitmap.getPixels(pixels, 0, width, 0, 0, width, height)
                val barcode =  Image(width, height, "RGB4")
                barcode.setData(pixels)
                val result = scanner?.scanImage(barcode.convert("Y800"))

                if (result != 0) {
                    val scanResult =  scanner?.results?.first()?.data?.trim()
                    handle.invoke(scanResult,true)
                    return
                }
            }
        }
        catch (e:Exception){
            handle.invoke(e.message,false)
        }
        handle.invoke("not result scanner",false)
    }
}