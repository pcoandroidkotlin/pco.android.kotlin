package xpertgroup.co.scannerqr.modules

import dagger.Module
import dagger.Provides
import xpertgroup.co.scannerqr.implementations.ZBarQR
import xpertgroup.co.scannerqr.interfaces.IScannerQR

@Module
open class ScannerQRModule {

    @Provides
    fun scannerOcr(): IScannerQR {
        return ZBarQR()
    }
}