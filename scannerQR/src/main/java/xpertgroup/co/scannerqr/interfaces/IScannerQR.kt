package xpertgroup.co.scannerqr.interfaces

import android.graphics.Bitmap

interface IScannerQR {
     fun  scan(bitmap: Bitmap?, handle: (String?, Boolean) -> Unit)
}

