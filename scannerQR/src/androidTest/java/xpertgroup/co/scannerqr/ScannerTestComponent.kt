package xpertgroup.co.scannerqr

import dagger.Component
import xpertgroup.co.scannerqr.interfaces.IScannerQR

@Component(modules = [ScannerQRTestModule::class])
interface ScannerTestComponent{
    fun scannerOcr(): IScannerQR
    fun inject(instance : ScannerQRTest)
}