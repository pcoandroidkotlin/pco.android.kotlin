package xpertgroup.co.scannerqr

import dagger.Module
import dagger.Provides
import xpertgroup.co.scannerqr.implementations.ZBarQR
import xpertgroup.co.scannerqr.interfaces.IScannerQR

@Module
class ScannerQRTestModule {

    @Provides
    fun scannerOcr(): IScannerQR {
        return ZBarQR()
    }
}