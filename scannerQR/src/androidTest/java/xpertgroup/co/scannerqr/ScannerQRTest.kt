package xpertgroup.co.scannerqr

import android.content.Context
import android.graphics.BitmapFactory
import android.support.test.InstrumentationRegistry.getInstrumentation
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import xpertgroup.co.scannerqr.interfaces.IScannerQR
import javax.inject.Inject


@RunWith(MockitoJUnitRunner::class)
class ScannerQRTest {

    @Inject
    lateinit var scannerQR: IScannerQR

    private lateinit var context: Context

    @Before
    fun setup(){
        context = getInstrumentation().context
        val component = DaggerScannerTestComponent.builder()
            .scannerQRTestModule(ScannerQRTestModule()).build()
        component.inject(this)
        assertNotNull(context)
    }

    @Test
    fun readOcr() {
        try {
            var result : String? = null
            var successResult  = false
            val bm = BitmapFactory.decodeResource(context.resources, R.drawable.qr_discount_1)
            scannerQR.scan(bm){ value: String?, success: Boolean ->
                result = value
                successResult = success
            }
            assertTrue(result,!result.isNullOrEmpty() && successResult)
        } catch (e: Exception) {
            error(e.message.toString())
        }
    }
}