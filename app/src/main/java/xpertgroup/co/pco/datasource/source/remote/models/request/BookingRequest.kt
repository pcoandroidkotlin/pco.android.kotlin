package xpertgroup.co.pco.datasource.source.remote.models.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BookingRequest(flightId : String,passengerNumber : Int,discountCode : String) {
    @SerializedName("userId")
    @Expose
    var flightId : String = flightId
    @SerializedName("userId")
    @Expose
    var passengerNumber : Int = passengerNumber
    @SerializedName("userId")
    @Expose
    var discountCode : String = discountCode
}