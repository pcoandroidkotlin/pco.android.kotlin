package xpertgroup.co.pco.datasource.source.remote.models.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CreateUserRequest( email :String,  name :String,  birthdayDate :String,  phone :String,  password :String) {
    @SerializedName("Email")
    @Expose
    var email =email
    @SerializedName("Nombre")
    @Expose
    var name = name
    @SerializedName("FechaNacimiento")
    @Expose
    var birthdayDate = birthdayDate
    @SerializedName("Celular")
    @Expose
    var phone = phone
    @SerializedName("Contrasena")
    @Expose
    var password = password
}