package xpertgroup.co.pco.datasource.source.remote.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResponseRemote<T:Any?> {
     @SerializedName("Resultado")
     @Expose
     var result:T?  = null
     @SerializedName("Respuesta")
     @Expose
     var response : Boolean = false
     @SerializedName("Mensaje")
     @Expose
     var message : Message? = null
}