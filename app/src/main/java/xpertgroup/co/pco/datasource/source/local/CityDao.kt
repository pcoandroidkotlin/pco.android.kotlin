package xpertgroup.co.pco.datasource.source.local

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import xpertgroup.co.pco.datasource.entities.City

@Dao
abstract  class CityDao {

    @Query("SELECT * FROM city")
    abstract fun getAll(): LiveData<List<City>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(cities: List<City>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(city: City)

    @Delete
    abstract fun deleteAll(cities: List<City>)

    @Query("DELETE FROM city")
    abstract fun deleteAll()

    @Delete
    abstract fun delete(city : City)
}