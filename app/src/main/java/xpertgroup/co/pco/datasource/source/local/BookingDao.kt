package xpertgroup.co.pco.datasource.source.local

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import xpertgroup.co.pco.datasource.entities.Booking

@Dao
abstract class BookingDao {

    @Query("SELECT * FROM booking")
    abstract fun getAll(): LiveData<List<Booking>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(bookings: List<Booking>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(booking: Booking)

    @Delete
    abstract fun deleteAll(cities: List<Booking>)

    @Query("DELETE FROM booking")
    abstract fun deleteAll()

    @Delete
    abstract fun delete(city : Booking)
}