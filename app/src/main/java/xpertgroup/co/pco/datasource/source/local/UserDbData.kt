package xpertgroup.co.pco.datasource.source.local

import android.arch.lifecycle.LiveData
import io.reactivex.Completable
import io.reactivex.Observable
import xpertgroup.co.pco.datasource.entities.User
import xpertgroup.co.pco.datasource.source.DataSource

class UserDbData(private val dao: UserDao) : DataSource<User> {

    override fun getAll(): LiveData<List<User>> {
        return dao.getAll()
    }

    override fun insertAll(list: List<User>) {
         dao.insertAll(list)
    }

    override fun insert(entity: User) {
         dao.insert(entity)
    }

    override fun delete(entity: User) {
         dao.delete(entity)
    }

    override fun deleteAll(list: List<User>) {
         dao.deleteAll(list)
    }

    override fun deleteAll() {
        dao.deleteAll()
    }

}