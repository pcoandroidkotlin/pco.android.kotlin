package xpertgroup.co.pco.datasource.source.remote

import retrofit2.http.Body
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import xpertgroup.co.pco.datasource.entities.User
import xpertgroup.co.pco.datasource.source.remote.models.request.CreateUserRequest
import xpertgroup.co.pco.datasource.source.remote.models.response.ResponseRemote

interface BookingApi {
    @FormUrlEncoded
    @POST("Usuario/Post")
    fun createUser(@Body post: CreateUserRequest): ResponseRemote<User>

}