package xpertgroup.co.pco.datasource.source

import android.arch.lifecycle.LiveData

interface DataSource<T : Any> {
    fun getAll(): LiveData<List<T>>
    fun insertAll(list: List<T>)
    fun insert(entity: T)
    fun delete(entity: T)
    fun deleteAll()
    fun deleteAll(list: List<T>)
}