package xpertgroup.co.pco.datasource.source.remote

import retrofit2.Call
import retrofit2.http.*
import xpertgroup.co.pco.datasource.entities.User
import xpertgroup.co.pco.datasource.source.remote.models.request.CreateUserRequest
import xpertgroup.co.pco.datasource.source.remote.models.request.LoginRequest
import xpertgroup.co.pco.datasource.source.remote.models.response.ResponseRemote

interface UserApi {

    @POST("Usuario/Post")
    fun createUser(@Body post: CreateUserRequest): Call<ResponseRemote<User>>

    @POST("Authentication/Post")
    fun login(@Body post: LoginRequest): Call<ResponseRemote<User>>

}