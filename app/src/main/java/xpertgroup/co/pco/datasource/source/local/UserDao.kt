package xpertgroup.co.pco.datasource.source.local

import android.arch.lifecycle.LiveData
import android.arch.persistence.db.SupportSQLiteQuery
import android.arch.persistence.room.*
import io.reactivex.Maybe
import xpertgroup.co.pco.datasource.entities.User

@Dao
abstract class UserDao  {

    @Query("SELECT * FROM user")
    abstract fun getAll(): LiveData<List<User>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(users: List<User>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(user: User)

    @Delete
    abstract fun deleteAll(users: List<User>)

    @Query("DELETE FROM user")
    abstract fun deleteAll()

    @Delete
    abstract fun delete(user :User)

}