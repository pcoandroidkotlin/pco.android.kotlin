package xpertgroup.co.pco.datasource.source.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import xpertgroup.co.pco.datasource.entities.Booking
import xpertgroup.co.pco.datasource.entities.City
import xpertgroup.co.pco.datasource.entities.User

@Database(
        entities = [
            Booking::class,
             City::class,
             User::class
        ],
        version = 1,
        exportSchema = false
)
//@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getBookingDao(): BookingDao
    abstract fun getCityDao(): CityDao
    abstract fun getUserDao(): UserDao

    companion object {

        private const val DB_NAME = "bookingFlight.db"

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase = INSTANCE
            ?: synchronized(this) {
            INSTANCE
                ?: buildDatabase(context.applicationContext).also { INSTANCE = it }
        }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context, AppDatabase::class.java,
                    DB_NAME
                )
                        .fallbackToDestructiveMigration()
                        .build()
    }
}
/*
class DateConverter {

    @TypeConverter
    fun toDate(value: Long?) = value?.let { Date(value) }

    @TypeConverter
    fun toLong(value: Date?) = value?.time
}*/