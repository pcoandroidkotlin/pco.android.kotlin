package xpertgroup.co.pco.datasource.source.local

import android.arch.lifecycle.LiveData
import xpertgroup.co.pco.datasource.entities.City
import xpertgroup.co.pco.datasource.source.DataSource

class CityDbData(private val dao: CityDao) : DataSource<City> {

    override fun getAll(): LiveData<List<City>> {
        return dao.getAll()
    }

    override fun insertAll(list: List<City>) {
        dao.insertAll(list)
    }

    override fun insert(entity: City) {
        dao.insert(entity)
    }

    override fun delete(entity: City) {
        dao.delete(entity)
    }

    override fun deleteAll(list: List<City>) {
        dao.deleteAll(list)
    }

    override fun deleteAll() {
        dao.deleteAll()
    }
}