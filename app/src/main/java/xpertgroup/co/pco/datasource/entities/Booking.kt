package xpertgroup.co.pco.datasource.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "booking")
data class Booking(@PrimaryKey var id: Int?,
                   var airLineId : Int?,
                   var airLine : String?,
                   var cityDeparturedId :  Int?,
                   var cityDepartured :  String?,
                   var cityArraivalId :  Int?,
                   var cityArraival:  Int?,
                   var departuredDate :  String?,
                   var arraivalDate :  String?,
                   var tariff :  Int?,
                   var stateId :  Int?,
                   var state :  String?,
                   var passengerNumber :  Int?)