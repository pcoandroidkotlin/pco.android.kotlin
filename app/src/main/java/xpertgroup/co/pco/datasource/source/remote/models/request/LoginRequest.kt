package xpertgroup.co.pco.datasource.source.remote.models.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginRequest(email:String, password:String) {
    @SerializedName("Email")
    @Expose
    var email = email
    @SerializedName("Contrasena")
    @Expose
    var password = password
}