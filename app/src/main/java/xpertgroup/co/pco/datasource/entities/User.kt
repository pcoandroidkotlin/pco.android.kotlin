package xpertgroup.co.pco.datasource.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "user")
class User  {
    @SerializedName("Id")
    @Expose
    @PrimaryKey
    var id : Int = 0

    @SerializedName("Email")
    @Expose
    var email : String?= null

    @SerializedName("Nombre")
    @Expose
    var name : String?= null

    @SerializedName("FechaNacimiento")
    @Expose
    var birthdayDate : String?= null

    @SerializedName("Celular")
    @Expose
    var phone : String?= null

    @SerializedName("Token")
    @Expose
    var token: String?= null
}