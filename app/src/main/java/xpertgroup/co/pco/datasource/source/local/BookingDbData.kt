package xpertgroup.co.pco.datasource.source.local

import android.arch.lifecycle.LiveData
import xpertgroup.co.pco.datasource.entities.Booking
import xpertgroup.co.pco.datasource.source.DataSource

class BookingDbData(private val dao: BookingDao) : DataSource<Booking> {

    override fun getAll(): LiveData<List<Booking>> {
        return dao.getAll()
    }

    override fun insertAll(list: List<Booking>) {
        dao.insertAll(list)
    }

    override fun insert(entity: Booking) {
        dao.insert(entity)
    }

    override fun delete(entity: Booking) {
        dao.delete(entity)
    }

    override fun deleteAll(list: List<Booking>) {
        dao.deleteAll(list)
    }

    override fun deleteAll() {
        dao.deleteAll()
    }
}