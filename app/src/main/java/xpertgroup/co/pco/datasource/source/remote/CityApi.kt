package xpertgroup.co.pco.datasource.source.remote

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import xpertgroup.co.pco.datasource.entities.City
import xpertgroup.co.pco.datasource.source.remote.models.response.ResponseRemote

interface CityApi {
    @GET("Ciudad/Get")
    fun getCities(@Header("token")  token:String): Call<ResponseRemote<List<City>>>
}