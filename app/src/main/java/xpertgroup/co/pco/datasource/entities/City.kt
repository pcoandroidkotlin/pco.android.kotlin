package xpertgroup.co.pco.datasource.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "city")
class City {
    @PrimaryKey
    @SerializedName("CiudadId")
    @Expose
    var id :Int = 0
    @SerializedName("Ciudad")
    @Expose
    var city : String? = null
    @SerializedName("PaisId")
    @Expose
    var countryId : Int? = null
    @SerializedName("Pais")
    @Expose
    var country : String? = null
}