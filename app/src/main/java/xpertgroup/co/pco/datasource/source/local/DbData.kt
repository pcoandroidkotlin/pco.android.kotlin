package xpertgroup.co.pco.datasource.source.local

import android.arch.persistence.db.SimpleSQLiteQuery
import xpertgroup.co.pco.datasource.source.DataSource
import xpertgroup.co.pco.datasource.entities.Booking
import xpertgroup.co.pco.datasource.entities.City
import xpertgroup.co.pco.datasource.entities.User
import xpertgroup.co.pco.App
import kotlin.reflect.KClass

object DbData {

   private  val db: AppDatabase by lazy { AppDatabase.getInstance(App.appContext()) }

    @Suppress("UNCHECKED_CAST")
    fun <Entity : Any> of(clazz: KClass<*>): DataSource<Entity> {
        return when (clazz) {
            Booking::class -> BookingDbData(db.getBookingDao())
            City::class -> CityDbData(db.getCityDao())
            User::class -> UserDbData(db.getUserDao())
            else -> throw IllegalArgumentException("Unsupported data type")
        } as DataSource<Entity>
    }

    fun clearDb() {
        db.clearAllTables()
    }

    fun sqlWhere(table: String, params: Map<String, String>): SimpleSQLiteQuery {
        var query = "SELECT * FROM $table"
        params.keys.forEachIndexed { i, s ->
            query += if (i == 0) " WHERE" else " AND"
            query += " $s = ?"
        }

        val args = params.values.toTypedArray()
        return SimpleSQLiteQuery(query, args)
    }
}