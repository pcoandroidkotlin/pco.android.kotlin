package xpertgroup.co.pco.datasource.repositories

import android.arch.lifecycle.LiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xpertgroup.co.pco.App.Companion.getString
import xpertgroup.co.pco.App.Companion.isNetworkAvailable
import xpertgroup.co.pco.R
import xpertgroup.co.pco.datasource.entities.User
import xpertgroup.co.pco.datasource.source.DataSource
import xpertgroup.co.pco.datasource.source.local.DbData
import xpertgroup.co.pco.datasource.source.remote.ApiService
import xpertgroup.co.pco.datasource.source.remote.UserApi
import xpertgroup.co.pco.datasource.source.remote.models.request.CreateUserRequest
import xpertgroup.co.pco.datasource.source.remote.models.request.LoginRequest
import xpertgroup.co.pco.datasource.source.remote.models.response.ResponseRemote
import xpertgroup.co.pco.utils.async.DoAsync

class UserRepository {

    private var userDao : DataSource<User> = DbData.of(User::class)
    private var userApi = ApiService.create(UserApi::class.java)
    private var allUsers: LiveData<List<User>> =userDao.getAll()


    fun createUser(user: CreateUserRequest,handlerComplete:()-> Unit,handleError : (String)-> Unit)
    {
        if (isNetworkAvailable()) {
            val call = userApi.createUser(user)
            call.enqueue(object : Callback<ResponseRemote<User>>{
                override fun onFailure(call: Call<ResponseRemote<User>>, t: Throwable) {
                    handleError(getString(R.string.generic_error_api))
                }
                override fun onResponse(call: Call<ResponseRemote<User>>, response: Response<ResponseRemote<User>>) {
                    val result = response.body()
                    if(response.body()?.result != null){
                        DoAsync({
                            userDao.deleteAll()
                            userDao.insertAll(listOf(result?.result!!))
                        },handlerComplete).execute()
                    }
                    else{
                        handleError(result?.message?.text!!)
                    }
                }
            })
        }
        else{
            handleError(getString(R.string.connect_server_error))
        }
    }

    fun login(user: LoginRequest, handleError : (String)-> Unit)  {
        if (isNetworkAvailable()) {
            val call = userApi.login(user)
            call.enqueue(object : Callback<ResponseRemote<User>>{
                override fun onFailure(call: Call<ResponseRemote<User>>, t: Throwable) {
                    handleError(getString(R.string.generic_error_api))
                }
                override fun onResponse(call: Call<ResponseRemote<User>>, response: Response<ResponseRemote<User>>) {
                    val result = response.body()
                    if(response.body()?.result != null){
                        DoAsync({
                            userDao.deleteAll()
                            userDao.insertAll(listOf(result?.result!!))
                        }).execute()
                    }
                    else{
                        handleError(result?.message?.text!!)
                    }
                }
            })
        }
        else{
            handleError(getString(R.string.connect_server_error))
        }
    }


    fun logout(handlerComplete:()-> Unit)
    {
        DoAsync({
            userDao.deleteAll()
        },handlerComplete).execute()

    }

    fun getAllUsers(): LiveData<List<User>> {
        return allUsers
    }
}