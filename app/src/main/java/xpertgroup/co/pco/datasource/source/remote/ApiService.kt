package xpertgroup.co.pco.datasource.source.remote

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object ApiService {

    private const val API_ENDPOINT = "http://reservasapi.azurewebsites.net/Reservas/"
    private  const val DATE_FORMAT = "yyyy/MM/dd"
    fun <S> create(clazz: Class<S>): S = Retrofit.Builder()
        .baseUrl(API_ENDPOINT)
        .client(OkHttpClient())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(
            GsonConverterFactory.create(
                GsonBuilder()
                    .setDateFormat(DATE_FORMAT)
                    .create()
            )).build()
        .create(clazz)

}