package xpertgroup.co.pco.datasource.exceptions

class ApiServiceException (message: String,cause:Throwable? = null ) : Exception( message, cause)
