package xpertgroup.co.pco.datasource.source.remote.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Message {
    @SerializedName("Codigo")
    @Expose
    var code : Int? = null
    @SerializedName("Texto")
    @Expose
    var text : String = ""
}