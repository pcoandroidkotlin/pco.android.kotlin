package xpertgroup.co.pco.datasource.repositories

import android.arch.lifecycle.LiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xpertgroup.co.pco.App
import xpertgroup.co.pco.R
import xpertgroup.co.pco.datasource.entities.City
import xpertgroup.co.pco.datasource.source.DataSource
import xpertgroup.co.pco.datasource.source.local.DbData
import xpertgroup.co.pco.datasource.source.remote.ApiService
import xpertgroup.co.pco.datasource.source.remote.CityApi
import xpertgroup.co.pco.datasource.source.remote.models.response.ResponseRemote
import xpertgroup.co.pco.utils.async.DoAsync

class CityRespository {

    private var cityDao: DataSource<City> = DbData.of(City::class)
    private var cityApi = ApiService.create(CityApi::class.java)
    private var allCities: LiveData<List<City>> = cityDao.getAll()


    fun getAllCities(): LiveData<List<City>> {
        return allCities
    }

    fun updateCities(token :String, handleError: (String) -> Unit) {
        if (App.isNetworkAvailable()) {
            val call = cityApi.getCities(token)
            call.enqueue(object : Callback<ResponseRemote<List<City>>> {
                override fun onFailure(call: Call<ResponseRemote<List<City>>>, t: Throwable) {
                    handleError(App.getString(R.string.generic_error_api))
                }

                override fun onResponse(
                    call: Call<ResponseRemote<List<City>>>,
                    response: Response<ResponseRemote<List<City>>>
                ) {
                    val result = response.body()
                    if (response.body()?.result != null) {
                        DoAsync({
                            cityDao.deleteAll()
                            cityDao.insertAll(result?.result!!)
                        }).execute()
                    } else {
                        handleError(result?.message?.text!!)
                    }
                }
            })
        } else {
            handleError(App.getString(R.string.connect_server_error))
        }
    }
}