package xpertgroup.co.pco.components

import dagger.Component
import xpertgroup.co.pco.datasource.repositories.UserRepository
import xpertgroup.co.pco.components.modules.UserModule
import xpertgroup.co.pco.viewmodels.UserViewModel

@Component(modules = [UserModule::class])
interface UserComponent {
    fun userRepository(): UserRepository
    fun inject(instance: UserViewModel)
}