package xpertgroup.co.pco.components.modules

import dagger.Module
import dagger.Provides
import xpertgroup.co.pco.datasource.repositories.UserRepository

@Module
class UserModule  {

    @Provides
    fun userRepository() : UserRepository {
        return UserRepository()
    }
}