package xpertgroup.co.pco.components.modules

import dagger.Module
import dagger.Provides
import xpertgroup.co.pco.datasource.repositories.CityRespository
import xpertgroup.co.pco.datasource.repositories.UserRepository

@Module
class CityModule {
    @Provides
    fun cityRepository() : CityRespository {
        return CityRespository()
    }
}