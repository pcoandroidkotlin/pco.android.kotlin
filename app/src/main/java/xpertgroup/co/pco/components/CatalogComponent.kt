package xpertgroup.co.pco.components

import dagger.Component
import xpertgroup.co.pco.components.modules.CityModule
import xpertgroup.co.pco.datasource.repositories.CityRespository
import xpertgroup.co.pco.viewmodels.CityViewModel

@Component(modules = [CityModule::class])
interface CatalogComponent {
    fun cityRepository(): CityRespository
    fun inject(instance: CityViewModel)
}