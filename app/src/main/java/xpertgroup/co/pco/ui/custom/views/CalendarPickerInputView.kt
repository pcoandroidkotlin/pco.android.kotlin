package xpertgroup.co.pco.ui.custom.views

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import kotlinx.android.synthetic.main.calendar_picker_input_view.view.*
import xpertgroup.co.pco.R
import java.util.*


class CalendarPickerInputView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : ConstraintLayout(context, attrs, defStyleAttr) {


    private val calendar = Calendar.getInstance()

    private val month = calendar.get(Calendar.MONTH)
    private val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
    private val year = calendar.get(Calendar.YEAR)
    private  var  yearRestriction :Int
    private  var  restrictionNextDay :Int
    private  var  lastRestrictionDay :Int

    init {
        LayoutInflater.from(context).inflate(R.layout.calendar_picker_input_view, this, true)
        val attributes  = context.obtainStyledAttributes(attrs, R.styleable.CalendarPickerInputView)
        val hint = attributes?.getString(R.styleable.CalendarPickerInputView_hint)!!
        yearRestriction = attributes.getInt(R.styleable.CalendarPickerInputView_lastRestrictionYear,0)
        restrictionNextDay = attributes.getInt(R.styleable.CalendarPickerInputView_restrictionNextDay,0)
        lastRestrictionDay = attributes.getInt(R.styleable.CalendarPickerInputView_lastRestrictionDay,0)

        date.hint = hint
        attributes.recycle()
        btnCaptureDate.setOnClickListener{
            startCaptureDate()
        }
    }

    companion object{
        private const val ZERO = "0"
        private const  val SPACER = "-"
    }

    @SuppressLint("SetTextI18n")
    private  fun startCaptureDate(){
       val datePicker =  DatePickerDialog(
            context,
            DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                val currentMonth = month + 1
                val formatDay = if (dayOfMonth < 10) ZERO + dayOfMonth.toString() else dayOfMonth.toString()
                val formatMonth = if (currentMonth < 10) ZERO + currentMonth.toString() else currentMonth.toString()
                date.setText(year.toString() + SPACER + formatMonth + SPACER + formatDay)
            },year, month, dayOfMonth
        )
        calendar.add(Calendar.YEAR,-1*yearRestriction)
        calendar.add(Calendar.DAY_OF_YEAR,-lastRestrictionDay)
        datePicker.datePicker.maxDate = calendar.time.time
        val calendarMin = Calendar.getInstance()
        calendarMin.add(Calendar.DAY_OF_YEAR,restrictionNextDay)
        datePicker.datePicker.minDate = calendarMin.time.time
        datePicker.show()
    }

    fun getText():String?{
        return date.text?.toString()
    }
}