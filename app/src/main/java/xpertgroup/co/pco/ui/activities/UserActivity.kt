package xpertgroup.co.pco.ui.activities

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import com.basgeekball.awesomevalidation.utility.RegexTemplate
import kotlinx.android.synthetic.main.user_activity.*
import xpertgroup.co.pco.App.Companion.activityHideActionBar
import xpertgroup.co.pco.App.Companion.fullScreenActivity
import xpertgroup.co.pco.App.Companion.hideKeyboard
import xpertgroup.co.pco.App.Companion.stringToBase64
import xpertgroup.co.pco.R
import xpertgroup.co.pco.datasource.source.remote.models.request.CreateUserRequest
import xpertgroup.co.pco.utils.validations.*
import xpertgroup.co.pco.utils.validations.notemptydatetime.ValidationCallbackNotEmptyDateTime
import xpertgroup.co.pco.utils.validations.notemptydatetime.ValidationConsecutiveNumbers
import xpertgroup.co.pco.utils.validations.notemptydatetime.ValidationErrorNotEmptyDateTime
import xpertgroup.co.pco.viewmodels.UserViewModel

class UserActivity :AppCompatActivity() {

    private lateinit var userViewModel: UserViewModel
    private val mAwesomeValidation = AwesomeValidation(ValidationStyle.COLORATION)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fullScreenActivity(this)
        activityHideActionBar(this)
        setContentView(R.layout.user_activity)
        hideKeyboard(this)
        validateConfig()
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        btnCreateUser.setOnClickListener{
            if(mAwesomeValidation.validate()){
                userViewModel.createUser(
                    CreateUserRequest(email.text.toString(),
                        name.text.toString(), birthDayDate.getText()!!,
                        phone.text.toString(),stringToBase64(password.text.toString()))
                    ,{
                        Toast.makeText(this,R.string.user_correct_register, Toast.LENGTH_SHORT).show()
                        this.finish()
                    },{
                        Toast.makeText(this,it, Toast.LENGTH_SHORT).show()
                    })
            }
        }
    }

    private  fun validateConfig(){
        AwesomeValidation.disableAutoFocusOnFirstFailure()
        mAwesomeValidation.addValidation(this, R.id.email, RegularExpression.EMAIL_EXPRESSION, R.string.incorrect_format_email)
        mAwesomeValidation.addValidation(this, R.id.password,
            ValidationConsecutiveNumbers(),R.string.incorrect_format_password)
        mAwesomeValidation.addValidation(this, R.id.name, RegexTemplate.NOT_EMPTY ,R.string.incorrect_format_name)
        mAwesomeValidation.addValidation(this, R.id.birthDayDate, ValidationNotEmptyDateTime(),ValidationCallbackNotEmptyDateTime(), ValidationErrorNotEmptyDateTime(),R.string.incorrect_format_birthday_date)
        mAwesomeValidation.addValidation(this, R.id.phone, ValidationMobilePhone() ,R.string.incorrect_format_mobile_phone)
    }

}