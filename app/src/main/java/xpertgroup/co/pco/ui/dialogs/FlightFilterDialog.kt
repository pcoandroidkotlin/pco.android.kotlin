package xpertgroup.co.pco.ui.dialogs

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import xpertgroup.co.pco.R
import xpertgroup.co.pco.datasource.entities.City
import xpertgroup.co.pco.viewmodels.CityViewModel

class FlightFilterDialog : DialogFragment() {

    private lateinit var cityViewModel: CityViewModel
    private  var cities: List<City>? = null
    private lateinit var token : String

    companion object{
        private const val TOKEN ="tokenDialog"
        fun newInstance(token:String):FlightFilterDialog{
            val dialogFragment = FlightFilterDialog()
            val bundle = Bundle()
            bundle.putString(TOKEN,token)
            dialogFragment.arguments = bundle
            return dialogFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_flight_filter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cityViewModel = ViewModelProviders.of(this).get(CityViewModel::class.java)
        cityViewModel.getAllCities().observe(this,
            Observer<List<City>> {
                cities = it
                if(!cities?.any()!!){
                    cityViewModel.updateCities(token) {itMessage->
                        Toast.makeText(context,itMessage, Toast.LENGTH_SHORT).show()
                    }
                }
            })
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        token = arguments?.getString(TOKEN)!!
        super.onViewStateRestored(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        arguments?.putString(TOKEN,token)
        super.onSaveInstanceState(outState)
    }
}