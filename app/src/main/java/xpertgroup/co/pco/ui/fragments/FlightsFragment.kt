package xpertgroup.co.pco.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_flights.*
import xpertgroup.co.pco.R
import xpertgroup.co.pco.ui.dialogs.FlightFilterDialog




class FlightsFragment : Fragment() {

    private lateinit var token : String

    companion object {
        private const val TOKEN ="tokenFragment"
        fun newInstance(token :String?): FlightsFragment {
            val fragment = FlightsFragment()
            val bundle = Bundle()
            bundle.putString(TOKEN,token)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_flights, container, false)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        token = arguments?.getString(TOKEN)!!
        super.onViewStateRestored(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        arguments?.putString(TOKEN,token)
        super.onSaveInstanceState(outState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        filterFlightFab.setOnClickListener {
            val dialog = FlightFilterDialog.newInstance(token)
            dialog.show(fragmentManager,FlightFilterDialog::class.java.name)

        }
    }

}