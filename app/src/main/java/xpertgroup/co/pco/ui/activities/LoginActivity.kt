package xpertgroup.co.pco.ui.activities

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import kotlinx.android.synthetic.main.login_activity.*
import xpertgroup.co.pco.App
import xpertgroup.co.pco.App.Companion.activityHideActionBar
import xpertgroup.co.pco.App.Companion.fullScreenActivity
import xpertgroup.co.pco.App.Companion.stringToBase64
import xpertgroup.co.pco.R
import xpertgroup.co.pco.datasource.entities.User
import xpertgroup.co.pco.datasource.source.remote.models.request.LoginRequest
import xpertgroup.co.pco.utils.validations.RegularExpression
import xpertgroup.co.pco.utils.validations.notemptydatetime.ValidationConsecutiveNumbers
import xpertgroup.co.pco.viewmodels.UserViewModel


class LoginActivity : AppCompatActivity() {

    private lateinit var userViewModel: UserViewModel
    private  var user: User? = null
    private val mAwesomeValidation = AwesomeValidation(ValidationStyle.COLORATION)

    companion object{
        private const val  RECORD_REQUEST_CODE = 10
        private  val  TAG = LoginActivity::class.java.name
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fullScreenActivity(this)
        activityHideActionBar(this)
        setContentView(R.layout.login_activity)
        App.hideKeyboard(this)
        validateConfig()

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        userViewModel.getAllUsers().observe(this,
            Observer<List<User>> {
                user = it?.firstOrNull()
                if(!user?.token.isNullOrEmpty()){
                    App.startActivity(this,MainActivity::class.java)
                }
            })

        linkSingUp.setOnClickListener{
            App.startActivity(this,UserActivity::class.java)
        }

        btnLogin.setOnClickListener {
            if(mAwesomeValidation.validate()){
                userViewModel.login(LoginRequest(email.text.toString(),stringToBase64(password.text.toString()))){
                    Toast.makeText(this,it,Toast.LENGTH_SHORT).show()
                }
            }
        }

        setupPermissions()

    }

    override fun onResume() {
        super.onResume()
        if(!user?.token.isNullOrEmpty()){
            App.startActivity(this,MainActivity::class.java)
        }
    }

    private fun setupPermissions() {
            makeRequest()
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
            arrayOf(Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE),
            RECORD_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            RECORD_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission has been denied by user")
                } else {
                    Log.i(TAG, "Permission has been granted by user")
                }
            }
        }
    }

    private  fun validateConfig(){
        AwesomeValidation.disableAutoFocusOnFirstFailure()
        mAwesomeValidation.addValidation(this, R.id.email, RegularExpression.EMAIL_EXPRESSION, R.string.incorrect_format_email)
        mAwesomeValidation.addValidation(this, R.id.password,
        ValidationConsecutiveNumbers(),R.string.incorrect_format_password)
    }
}