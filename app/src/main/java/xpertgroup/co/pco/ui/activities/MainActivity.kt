package xpertgroup.co.pco.ui.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import android.support.design.widget.NavigationView
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import xpertgroup.co.pco.App
import xpertgroup.co.pco.App.Companion.fragmentReplace
import xpertgroup.co.pco.App.Companion.goToMain
import xpertgroup.co.pco.R
import xpertgroup.co.pco.datasource.entities.User
import xpertgroup.co.pco.ui.fragments.FlightsFragment
import xpertgroup.co.pco.viewmodels.UserViewModel

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    private lateinit var userViewModel: UserViewModel
    private  var user: User? = null
    private  val  mapActionNav : MutableMap<Int,(User)-> Unit> = mutableMapOf(Pair(R.id.nav_flights,{it->
        fragmentReplace(this,FlightsFragment.newInstance(it.token),R.id.containerFragment,false)
    }))


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.fullScreenActivity(this)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )

        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        userViewModel.getAllUsers().observe(this,
            Observer<List<User>> {
                user = it?.firstOrNull()
                if(user?.token.isNullOrEmpty()){
                    onBackPressed()
                }
                else{
                    mapActionNav[R.id.nav_flights]?.invoke(user!!)
                }
            })
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else if(!user?.token.isNullOrEmpty()){
            goToMain(this)
        }else{
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                userViewModel.logout {
                    user?.token = ""
                    Toast.makeText(this,R.string.close_session_correct,Toast.LENGTH_LONG).show()
                }
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        mapActionNav[item.itemId]?.invoke(user!!)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }


}
