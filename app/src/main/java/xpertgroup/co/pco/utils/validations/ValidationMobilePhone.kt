package xpertgroup.co.pco.utils.validations

import com.basgeekball.awesomevalidation.utility.custom.SimpleCustomValidation

class ValidationMobilePhone :  SimpleCustomValidation {
    override fun compare(p0: String?): Boolean {
        if(p0?.firstOrNull() != '3'){
            return  false
        }

        if(p0.length != 10){
            return  false
        }

        return p0.toLongOrNull() != null
    }
}