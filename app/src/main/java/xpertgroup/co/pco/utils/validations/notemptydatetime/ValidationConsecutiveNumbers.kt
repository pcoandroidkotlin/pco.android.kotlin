package xpertgroup.co.pco.utils.validations.notemptydatetime

import com.basgeekball.awesomevalidation.utility.custom.SimpleCustomValidation

class ValidationConsecutiveNumbers : SimpleCustomValidation {
    override fun compare(value: String?): Boolean {
        val array =value?.toCharArray()
        var captureConsecutive  = 0

        if(value?.length != 4){
            return false
        }

        for (i in 1 until array?.size!!) {

            if (array[i].isDigit() && array[i - 1].isDigit() &&  array[i].toInt() -1 == array[i -1].toInt()) {
                captureConsecutive++
            }

        }

        if(captureConsecutive == array.size-1){
            return false
        }
        captureConsecutive = 0

        for (i in array.size -1 downTo 1 ) {
            if (array[i].isDigit() && array[i - 1].isDigit() && array[i].toInt()  == array[i -1].toInt()-1) {
                captureConsecutive++
            }
        }

        return captureConsecutive != array.size-1
    }
}