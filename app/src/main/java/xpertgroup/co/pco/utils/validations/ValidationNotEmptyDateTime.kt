package xpertgroup.co.pco.utils.validations

import com.basgeekball.awesomevalidation.ValidationHolder
import com.basgeekball.awesomevalidation.utility.custom.CustomValidation
import xpertgroup.co.pco.ui.custom.views.CalendarPickerInputView


class ValidationNotEmptyDateTime  : CustomValidation{
    override fun compare(p0: ValidationHolder?): Boolean {
        return (!(p0?.view as CalendarPickerInputView).getText().isNullOrEmpty())
    }

}