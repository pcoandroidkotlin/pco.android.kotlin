package xpertgroup.co.pco.utils.validations.notemptydatetime

import android.graphics.Color
import com.basgeekball.awesomevalidation.ValidationHolder
import com.basgeekball.awesomevalidation.utility.custom.CustomErrorReset
import android.widget.TextView
import kotlinx.android.synthetic.main.calendar_picker_input_view.view.*
import xpertgroup.co.pco.ui.custom.views.CalendarPickerInputView


class ValidationErrorNotEmptyDateTime : CustomErrorReset {
    override fun reset(validationHolder: ValidationHolder?) {
        val textViewError = (validationHolder?.view as CalendarPickerInputView).date as TextView
        textViewError.error = null
        textViewError.setTextColor(Color.BLACK)
    }
}