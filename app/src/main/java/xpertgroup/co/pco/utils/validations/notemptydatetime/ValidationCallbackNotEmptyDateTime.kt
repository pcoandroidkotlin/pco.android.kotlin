package xpertgroup.co.pco.utils.validations.notemptydatetime

import android.graphics.Color
import android.widget.TextView
import com.basgeekball.awesomevalidation.ValidationHolder
import com.basgeekball.awesomevalidation.utility.custom.CustomValidationCallback
import kotlinx.android.synthetic.main.calendar_picker_input_view.view.*
import xpertgroup.co.pco.ui.custom.views.CalendarPickerInputView

class ValidationCallbackNotEmptyDateTime : CustomValidationCallback {

    override fun execute(validationHolder: ValidationHolder?) {
        val textViewError = (validationHolder?.view as CalendarPickerInputView).date as TextView
        textViewError.error = validationHolder.errMsg
        textViewError.setTextColor(Color.BLACK)
    }

}