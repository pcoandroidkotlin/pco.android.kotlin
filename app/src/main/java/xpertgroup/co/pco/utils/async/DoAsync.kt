package xpertgroup.co.pco.utils.async

import android.os.AsyncTask

class DoAsync (val handler: () -> Unit,val handleComplete: ()-> Unit = {}) : AsyncTask<Void, Void, Void>() {
    override fun doInBackground(vararg params: Void?): Void? {
        handler()
        return null
    }

    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        handleComplete()
    }

}