package xpertgroup.co.pco

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.support.annotation.IdRes
import android.support.v4.app.Fragment



class App : Application() {

    companion object {

        lateinit var instance: App

        fun appContext(): Context = instance.applicationContext


        fun isNetworkAvailable(): Boolean {
            val connManager = instance.applicationContext
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            val info = connManager.activeNetworkInfo
            return info?.isConnected ?: false
        }

        fun activityHideActionBar(activity:AppCompatActivity){
            activity.run {
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                supportActionBar?.hide()
            }
        }

        fun getString(@StringRes id: Int) : String{
          return instance.applicationContext.resources.getString(id)
        }


        fun hideKeyboard(activity: Activity) {
            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = activity.currentFocus
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        fun stringToBase64(text : String) : String{
            val data = text.toByteArray(charset("UTF-8"))
            return  Base64.encodeToString(data, Base64.DEFAULT)
        }

        fun <T:AppCompatActivity>startActivity(context : Context, activityType : Class<T> ){
            val intent = Intent(context, activityType)
            context.startActivity(intent)
        }

        fun fullScreenActivity(activity : AppCompatActivity){
            activity.run {
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                window.setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN)
            }
        }

        fun goToMain(context: Context){
            val startMain = Intent(Intent.ACTION_MAIN)
            startMain.addCategory(Intent.CATEGORY_HOME)
            startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(startMain)
        }

        fun fragmentReplace(activity: AppCompatActivity, fragment : Fragment, @IdRes id:Int, addToBackStack: Boolean){
            val transaction = activity.supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(R.anim.fragment_anim_alpha_in, R.anim.abc_fade_out)
            transaction.replace(id, fragment)
            if(addToBackStack){
                transaction.addToBackStack(fragment::class.java.name)
            }
            transaction.commit()
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}