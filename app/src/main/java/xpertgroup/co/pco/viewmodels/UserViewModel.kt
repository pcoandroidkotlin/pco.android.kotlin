package xpertgroup.co.pco.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import xpertgroup.co.pco.components.DaggerUserComponent
import xpertgroup.co.pco.datasource.entities.User
import xpertgroup.co.pco.datasource.repositories.UserRepository
import xpertgroup.co.pco.datasource.source.remote.models.request.CreateUserRequest
import xpertgroup.co.pco.datasource.source.remote.models.request.LoginRequest
import javax.inject.Inject

class UserViewModel(application: Application) : AndroidViewModel(application) {

    @Inject
    lateinit var userRepository : UserRepository

    init {
        val component = DaggerUserComponent.builder().build()
        component.inject(this)
    }

    fun getAllUsers():LiveData<List<User>>{
        return userRepository.getAllUsers()
    }

    fun login(user: LoginRequest, handleError: (String)-> Unit){
        userRepository.login(user,handleError)
    }

    fun createUser(user: CreateUserRequest,handlerComplete:()->Unit,handleError: (String)-> Unit){
        userRepository.createUser(user,handlerComplete,handleError)
    }

    fun logout(handlerComplete:()->Unit){
        userRepository.logout(handlerComplete)
    }
}