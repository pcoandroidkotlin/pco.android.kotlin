package xpertgroup.co.pco.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import xpertgroup.co.pco.components.DaggerCatalogComponent
import xpertgroup.co.pco.datasource.entities.City
import xpertgroup.co.pco.datasource.repositories.CityRespository
import javax.inject.Inject

class CityViewModel (application: Application) : AndroidViewModel(application) {

    @Inject
    lateinit var cityRepository: CityRespository

    init {
        val component = DaggerCatalogComponent.builder().build()
        component.inject(this)
    }

    fun getAllCities() : LiveData<List<City>>{
        return cityRepository.getAllCities()
    }

    fun updateCities(token:String, handleError: (String) -> Unit ){
        cityRepository.updateCities(token,handleError)
    }
}